# Remark template

### Markdown formatting

#### Front page

To create front page, at the top of the file we should have:

```
class: center, middle, intro

## DOCUMENT_TITLE

---
```

#### Page break

We should indicate in a markdown file, when given page should end. Following
syntax is used for page break:

```
---
```

#### Last page

To create last page, at the top of the file we should have:

```
class: center, middle, outro

## Title

---
```

## Usage

* execute the `create-html.sh` script:

> replace `"Progress report"` and `progress-report.md` with desired title and
> source file name
```
./remark-template/scripts/create-html.sh  "Progress report" progress-report.md
```

### Preview the HTML file

> Chrome / Chromium browser is recommended

* start the HTTP server in directory containing generated html:

```
python3 -m http.server 8080
```

* Enter localhost `http://127.0.0.1:8080/` and view the HTML file in browser.

### Generate PDF file

For now, use a browser print option (`ctrl+p` in google chrome).
