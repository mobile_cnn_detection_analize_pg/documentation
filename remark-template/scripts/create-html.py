import sys
import os.path
import shutil
import re

def usage():
    print(f"{sys.argv[0]} TITLE MARKDOWN_FILE")
    print("Example:")
    print(f"{sys.argv[0]} \"Progress raport\" progress-raport.md")

if len(sys.argv) != 3:
    usage()
    sys.exit(1)

TEMPLATE_HTML = "template.html"
TITLE = sys.argv[1]
MARKDOWN_FILE = sys.argv[2]

if not os.path.exists(MARKDOWN_FILE):
    print(f"Invalid file name: \"{MARKDOWN_FILE}\"")
    usage()
    sys.exit(1)

OUT_DIR = os.path.dirname(os.path.realpath(MARKDOWN_FILE))

MARKDOWN_FILE=os.path.basename(MARKDOWN_FILE)

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

OUTPUT_HTML = '.'.join([os.path.splitext(MARKDOWN_FILE)[0],"html"])

with open(f"{SCRIPT_DIR}/../{TEMPLATE_HTML}", "r") as template:
    temp_content = template.readlines()
with open(f"{OUT_DIR}/{OUTPUT_HTML}", "w") as output:
    for line in temp_content:
        tmp = re.sub(r"@@TITLE@@", f"{TITLE}", line)
        tmp = re.sub(r"@@MARKDOWN_FILE@@", f"{MARKDOWN_FILE}", tmp)
        output.write(tmp)
