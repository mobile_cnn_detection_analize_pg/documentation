#!/bin/bash

usage() {
  echo $0 TITLE MARKDOWN_FILE
  echo "Example:"
  echo "$0 \"Progress raport\" progress-raport.md"
}

if [ $# -ne 2 ]; then
  usage
  exit 1
fi

TEMPLATE_HTML="template.html"
TITLE="$1"
MARKDOWN_FILE="$2"

if [ ! -f $MARKDOWN_FILE ]; then
  echo "Invalid file name: \"$MARKDOWN_FILE\""
  usage
  exit 1
fi

OUT_DIR="$(dirname $(readlink -f $MARKDOWN_FILE))"
MARKDOWN_FILE="$(basename $MARKDOWN_FILE)"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
OUTPUT_HTML=${MARKDOWN_FILE%.*}.html

cp $SCRIPT_DIR/../$TEMPLATE_HTML $OUT_DIR/$OUTPUT_HTML
sed -e "s|@@TITLE@@|$TITLE|" -i $OUT_DIR/$OUTPUT_HTML
sed -e "s|@@MARKDOWN_FILE@@|$MARKDOWN_FILE|" -i $OUT_DIR/$OUTPUT_HTML
