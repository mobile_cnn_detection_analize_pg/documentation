class: intro, center, middle
## Analiza osiągów lekkich sieci konwolucyjnych w kontekście segmentacji obiektów na urządzeniach mobilnych
### Dokumentacja

#### Autorzy:
##### Maciej Jabłoński, Jakub Łęcki, Marek Hering

---

#### Streszczenie: <>

.pure-table-vertical[
| Wersja:           |   |
|-------------------|---|
| Data wydania:     |   |
| Redaktor:         |   |
| Współautorzy:     |   |
| Etap/zadanie:     |   |
| Nazwa pliku:      |   |
| Status poufności: |   |
]

---
#### Historia zmian

.pure-table[
| Wersja | Data | Opis zmiany |
|--------|------|-------------|
|        |     |              |
]

#### Spis treści 
1. Wstęp 3
    1. Cel dokumentu **4**
    1. Zakres dokumentu **4**
    1. Bibliografia **4**
2. Rozdział 1 **4**
    1. Podrozdział 1 **5**
        1. Sekcja 1 **5**

---

## 1. Wstęp

### 1.1. Cel dokumentu

Celem dokumentu jest...

### 1.2. Zakres dokumentu

W zakresie dokumentu lokuje się…

### 1.3. Bibliografia

[Nazwa1]	Opis 1

[Nazwa2]	Opis 2

[Nazwa3]	Opis 3

---

## 2. Rozdział 1

akapit

### 2.1. Podrozdział 1

akapit


#### 2.1.1. Sekcja 1
akapit

*  wypunktowanie poziom 1
   *  wypunktowanie poziom 2
   * wypunktowanie poziom 2

Poniżej numerowanie
1. punkt
1. punkt
1. punkt

Poniżej definicja pojęcia

```bash
Pojęcie – definicja
```
akapit i odwołanie do pozycji bibliografii poprzez odsyłacz do zakładki [Nazwa1](#3).

---

.center[
.image-40[![](img/sample.png)]
> **Rysunek 1** Podpis rysunku
]

</br>
**Tabela 1** Nazwa tabeli

.pure-table[
| Nagłówek 1 | Nagłówek 1 | Nagłówek 1 |
|:----------:|:----------:|:----------:|
|    tekst   |   tekst    |    tekst   |
]
